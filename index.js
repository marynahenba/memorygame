const cards = document.querySelectorAll('.card');
const resetBtn = document.querySelector('.reset-btn');
let flippedCards = [];
let counter = 0;

resetBtn.addEventListener('click', function () {
	cards.forEach((card) => {
		card.classList.remove('is-flipped');
	});
	flippedCards = [];
	resetCards();
});

(function flipCard() {
	cards.forEach((card) => {
		card.addEventListener('click', function () {
			card.classList.add('is-flipped');
			flippedCards.push(card);
			if (flippedCards.length === 2) {
				checkMatches();
				flippedCards = [];
			}
		});
	});
})();

function checkMatches() {
	const [firstCard, secondCard] = flippedCards;
	cards.forEach((card) => {
		card.classList.add('blocked');
	});
	if (firstCard.getAttribute('animal') === secondCard.getAttribute('animal')) {
		firstCard.classList.replace('is-flipped', 'matched');
		secondCard.classList.replace('is-flipped', 'matched');
		flippedCards = [];
		cards.forEach((card) => {
			card.classList.remove('blocked');
		});
		counter++;
		if (counter === cards.length / 2) {
			setTimeout(() => {
				counter = 0;
				resetCards();
			}, 900);
		}
	} else {
		setTimeout(() => {
			cards.forEach((card) => {
				card.classList.remove('is-flipped');
				card.classList.remove('blocked');
			});
		}, 800);
	}
}

function resetCards() {
	cards.forEach((card) => {
		card.classList.remove('matched');
		const randomNumber = Math.floor(Math.random() * cards.length) + 1;
		setTimeout(() => {
			card.style.order = `${randomNumber}`;
		}, 200);
	});
}
resetCards();
